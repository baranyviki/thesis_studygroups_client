export interface RegistrationModel{
    firstName: string,
    lastName:string,
    birthday:Date,
    gendertype: number,
    email:string,
    messengerName:string,
    instagramName:string,
    username:string,
    password:string,
    image:File,
    // gradeBookExport:File,
    // CourseExport:File
    // gradeBookExport:File,
    // CourseExport:File
}