export interface GeneralSelectionItem {
    id: number,
    displayName: string
}